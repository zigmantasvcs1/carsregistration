﻿namespace CarsRegistrationApp
{
	public class CarRegistration
	{
		private static Dictionary<string, string> carRegistry = new Dictionary<string, string>();
		public static IReadOnlyDictionary<string, string> Cars => carRegistry;

		/// <summary>
		/// Registers a car with a given license plate and owner.
		/// </summary>
		/// <param name="licensePlate">The license plate of the car to be registered.</param>
		/// <param name="owner">The name of the owner of the car to be registered.</param>
		/// <returns>True if the car was registered successfully; False if a car with the same
		/// license plate is already registered.</returns>
		public static bool RegisterCar(string licensePlate, string owner)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Retrieves the owner of a car with a given license plate.
		/// </summary>
		/// <param name="licensePlate">The license plate of the car.</param>
		/// <param name="owner">Output parameter to hold the name of the owner if found.</param>
		/// <returns>True if an owner with the provided license plate was found; False otherwise.</returns>
		public static bool GetOwner(string licensePlate, out string owner)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Retrieves all cars currently registered.
		/// </summary>
		/// <returns>A string representation of all registered cars.
		/// If no cars are registered, returns a message stating so.</returns>
		public static string GetAllCars()
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Saves the current state of the car registry to a file.
		/// </summary>
		/// <param name="filePath">The path of the file where the car registry data will be saved.</param>
		public static void SaveToFile(string filePath)
		{
			throw new NotImplementedException();
		}

		/// <summary>
		/// Loads car registry data from a specified file.
		/// The file should contain lines with a license plate and owner, separated by a comma. 
		/// Existing data in the carRegistry dictionary is cleared before loading the new data.
		/// </summary>
		/// <param name="filePath">The path to the file containing the car registry data.</param>
		public static void LoadFromFile(string filePath)
		{
			throw new NotImplementedException();
		}
	}
}
