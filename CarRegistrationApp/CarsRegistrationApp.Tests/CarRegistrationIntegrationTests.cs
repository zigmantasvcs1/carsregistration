﻿namespace CarsRegistrationApp.Tests
{
	[TestClass]
	public class ProgramTests
	{
		private StringWriter stringWriter;
		private StringReader stringReader;

		[TestInitialize]
		public void TestInitialize()
		{
			stringWriter = new StringWriter();
			Console.SetOut(stringWriter);
		}

		[TestCleanup]
		public void TestCleanup()
		{
			Console.SetIn(new StreamReader(Console.OpenStandardInput()));
			Console.SetOut(new StreamWriter(Console.OpenStandardOutput()));
			stringWriter = null;
			stringReader = null;
		}

		[TestMethod]
		public void RunShouldPrintMenuAndExitWhenChoiceIs6()
		{
			// Arrange
			var input = "6";
			stringReader = new StringReader(input);
			Console.SetIn(stringReader);

			// Act
			CarRegistrationUI.Run();

			// Assert
			var output = stringWriter.ToString();
			Assert.IsTrue(output.Contains("Car Registration Menu:"));
			Assert.IsTrue(output.Contains("1. Register Car"));
			Assert.IsTrue(output.Contains("2. Get Owner"));
			Assert.IsTrue(output.Contains("3. Print All Cars"));
			Assert.IsTrue(output.Contains("4. Save to File"));
			Assert.IsTrue(output.Contains("5. Load from File"));
			Assert.IsTrue(output.Contains("6. Exit"));
			Assert.IsTrue(output.Contains("Enter your choice: "));
		}

		[TestMethod]
		public void RunShouldPrintInvalidChoiceWhenChoiceIsNot1To6()
		{
			// Arrange
			var input = "7\n6";
			stringReader = new StringReader(input);
			Console.SetIn(stringReader);

			// Act
			CarRegistrationUI.Run();

			// Assert
			var output = stringWriter.ToString();
			Assert.IsTrue(output.Contains("Invalid choice. Please try again."));
		}

		[TestMethod]
		public void RunShouldRegisterCarWhenChoiceIs1()
		{
			// Arrange
			var licensePlate = "ABC123";
			var owner = "John Doe";
			var input = $"1\n{licensePlate}\n{owner}\n6"; // Choose 1, then provide the license plate and owner, then choose 6 to exit
			stringReader = new StringReader(input);
			Console.SetIn(stringReader);

			// Act
			CarRegistrationUI.Run();

			// Assert
			Assert.IsTrue(CarRegistration.Cars.ContainsKey(licensePlate));
			Assert.AreEqual(owner, CarRegistration.Cars[licensePlate]);
		}

		[TestMethod]
		public void RunShouldPrintOwnerWhenChoiceIs2()
		{
			// Arrange
			var licensePlate = "ABC123";
			var owner = "John Doe";
			CarRegistration.RegisterCar(licensePlate, owner);
			var input = $"2\n{licensePlate}\n6"; // Choose 2, then provide the license plate, then choose 6 to exit
			stringReader = new StringReader(input);
			Console.SetIn(stringReader);

			// Act
			CarRegistrationUI.Run();

			// Assert
			var output = stringWriter.ToString();
			Assert.IsTrue(output.Contains(owner));
		}

		[TestMethod]
		public void RunShouldPrintAllCarsWhenChoiceIs3()
		{
			// Arrange
			var licensePlate1 = "ABC123";
			var owner1 = "John Doe";
			CarRegistration.RegisterCar(licensePlate1, owner1);

			var licensePlate2 = "XYZ789";
			var owner2 = "Jane Smith";
			CarRegistration.RegisterCar(licensePlate2, owner2);

			var input = "3\n6"; // Choose 3 to print all cars, then choose 6 to exit
			stringReader = new StringReader(input);
			Console.SetIn(stringReader);

			// Act
			CarRegistrationUI.Run();

			// Assert
			var output = stringWriter.ToString();
			Assert.IsTrue(output.Contains(licensePlate1));
			Assert.IsTrue(output.Contains(owner1));
			Assert.IsTrue(output.Contains(licensePlate2));
			Assert.IsTrue(output.Contains(owner2));
		}

		[TestMethod]
		public void Run_ShouldSaveToFile_WhenChoiceIs4()
		{
			// Arrange
			var licensePlate = "ABC123";
			var owner = "John Doe";
			CarRegistration.RegisterCar(licensePlate, owner);

			var filePath = Path.GetTempFileName(); // Use a temporary file to avoid affecting the real file system
			var input = $"4\n{filePath}\n6"; // Choose 4 to save to file, then choose 6 to exit
			stringReader = new StringReader(input);
			Console.SetIn(stringReader);

			// Act
			CarRegistrationUI.Run();

			// Assert
			Assert.IsTrue(File.Exists(filePath)); // Check if the file was created

			var fileContents = File.ReadAllText(filePath); // Read the contents of the file
			Assert.IsTrue(fileContents.Contains($"{licensePlate},{owner}")); // Check if the contents are correct

			// Cleanup
			File.Delete(filePath); // Delete the temporary file
		}

		[TestMethod]
		public void RunShouldLoadFromFileWhenChoiceIs5()
		{
			// Arrange
			var licensePlate = "ABC123";
			var owner = "John Doe";
			CarRegistration.RegisterCar(licensePlate, owner);

			var filePath = Path.GetTempFileName(); // Use a temporary file to avoid affecting the real file system

			// Save to file
			CarRegistration.SaveToFile(filePath);

			var input = $"5\n{filePath}\n6"; // Choose 5 to load from file, then choose 6 to exit
			stringReader = new StringReader(input);
			Console.SetIn(stringReader);

			// Act
			CarRegistrationUI.Run();

			// Assert
			Assert.IsTrue(CarRegistration.GetOwner(licensePlate, out var loadedOwner)); // Check if the car was loaded
			Assert.AreEqual(owner, loadedOwner); // Check if the owner is correct

			// Cleanup
			File.Delete(filePath); // Delete the temporary file
		}
	}
}
